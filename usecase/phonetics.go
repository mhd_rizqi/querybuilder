package usecase

type PhoneticsRepo interface {
	FetchPhonetics(keyword string) []string
}

type PhoneticsUseCase struct {
	pRepo PhoneticsRepo
}

func (e *PhoneticsUseCase) GetKeywordPhonetics(keyword string) []string {
	return []string{"xiaomi", "xiomi", "siaomi"}
}
