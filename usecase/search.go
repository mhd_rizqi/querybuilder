package usecase

import "gitlab.com/mhd_rizqi/querybuilder/entity"

type QueryConfigRepo interface {
	GetQueryConfig(id string) string
}

type SearchEngineRepo interface {
	ExecuteSearch(query string) string
}

type SearchUseCase struct {
	qcRepo QueryConfigRepo
	seRepo SearchEngineRepo
	pRepo  PhoneticsRepo
}

func NewSearchUseCase(qcRepo QueryConfigRepo, seRepo SearchEngineRepo, pRepo PhoneticsRepo) *SearchUseCase {
	return &SearchUseCase{qcRepo: qcRepo, seRepo: seRepo, pRepo: pRepo}
}

func (uc *SearchUseCase) Search(id string, keyword string, page int, size int) {
	// 1. Get the configuration from repository
	config := uc.qcRepo.GetQueryConfig(id)

	// 2. Build the query from the configuration
	query := entity.Query{
		Body:        config,
		Keyword:     keyword,
		Page:        page,
		Size:        size,
		PhoneticsUC: &PhoneticsUseCase{pRepo: uc.pRepo},
	}
	query.BuildQuery()

	// 3. After the query was built, then execute search by using the query body
	uc.seRepo.ExecuteSearch(query.Body)
}
