package main

import (
	"gitlab.com/mhd_rizqi/querybuilder/repo"
	"gitlab.com/mhd_rizqi/querybuilder/usecase"
)

func main() {
	searchUC := usecase.NewSearchUseCase(&repo.QueryConfigRepo{}, &repo.SearchEngineRepo{}, &repo.PhoneticsRepo{})

	searchUC.Search("sample", "siomi", 4, 20)
}
