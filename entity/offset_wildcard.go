package entity

import (
	"fmt"
	"strconv"
)

type OffsetWildcard struct {
	Wildcard
	page int
	size int
}

func NewOffsetWildcard(id string, page int, size int) *OffsetWildcard {
	return &OffsetWildcard{
		Wildcard: Wildcard{Id: id},
		page:     page,
		size:     size,
	}
}

func (e *OffsetWildcard) resolve() Wildcard {
	fmt.Println("Offset wildcard resolved!")
	e.Wildcard.Value = strconv.Itoa((e.page - 1) * e.size)
	return e.Wildcard
}
