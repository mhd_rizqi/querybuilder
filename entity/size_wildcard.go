package entity

import (
	"fmt"
	"strconv"
)

type SizeWildcard struct {
	Wildcard
	size int
}

func NewSizeWildcard(id string, size int) *SizeWildcard {
	return &SizeWildcard{
		Wildcard: Wildcard{Id: id},
		size:     size,
	}
}

func (e *SizeWildcard) resolve() Wildcard {
	fmt.Println("Size wildcard resolved!")
	e.Wildcard.Value = strconv.Itoa(e.size)
	return e.Wildcard
}
