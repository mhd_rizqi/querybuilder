package entity

type Wildcard struct {
	Id    string
	Value string
}

type WildcardResolver interface {
	resolve() Wildcard
}
