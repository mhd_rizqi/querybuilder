package entity

import "fmt"

type Query struct {
	Body      string
	Keyword   string
	Page      int
	Size      int
	Wildcards []Wildcard

	PhoneticsUC PhoneticsUseCase
}

type PhoneticsUseCase interface {
	GetKeywordPhonetics(keyword string) []string
}

func (e *Query) BuildQuery() {
	// 1. scan wildcards from Body
	rawWildcards := e.scanWildcards()

	// 2. resolve wildcards
	e.resolve(rawWildcards)

	// 3. update Body with resolved wildcards
	fmt.Println("query body is built!")
	e.resolveQueryBody()
}

func (e *Query) scanWildcards() []string {
	return []string{"<KEYWORDS>", "<OFFSET>", "<SIZE>", "<PHONETICS>"}
}

func (e *Query) resolve(wildcards []string) {
	for _, v := range wildcards {
		switch v {
		case "<KEYWORDS>":
			e.Wildcards = append(e.Wildcards, NewKeywordWildcard(v, e.Keyword).resolve())
		case "<OFFSET>":
			e.Wildcards = append(e.Wildcards, NewOffsetWildcard(v, e.Page, e.Size).resolve())
		case "<SIZE>":
			e.Wildcards = append(e.Wildcards, NewSizeWildcard(v, e.Size).resolve())
		case "<PHONETICS>":
			e.Wildcards = append(e.Wildcards, NewPhoneticsWildcard(v, e.PhoneticsUC.GetKeywordPhonetics(e.Keyword)).resolve())
		}
	}
}

func (e *Query) resolveQueryBody() {
	for _, v := range e.Wildcards {
		fmt.Println(v.Value)
	}
}
