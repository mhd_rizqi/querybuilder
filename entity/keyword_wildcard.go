package entity

import "fmt"

type KeywordWildcard struct {
	Wildcard
	keyword string
}

func NewKeywordWildcard(id string, keyword string) *KeywordWildcard {
	return &KeywordWildcard{
		Wildcard: Wildcard{Id: id},
		keyword:  keyword,
	}
}

func (e *KeywordWildcard) resolve() Wildcard {
	fmt.Println("Keyword wildcard resolved!")
	e.Wildcard.Value = e.keyword
	return e.Wildcard
}
