package entity

import (
	"fmt"
	"strings"
)

type PhoneticsWildcard struct {
	Wildcard
	phonetics []string
}

func NewPhoneticsWildcard(id string, phonetics []string) *PhoneticsWildcard {
	return &PhoneticsWildcard{
		Wildcard:  Wildcard{Id: id},
		phonetics: phonetics,
	}
}

func (e *PhoneticsWildcard) resolve() Wildcard {
	fmt.Println("Phonetics wildcard resolved!")
	var phoneticsQuery strings.Builder

	for _, v := range e.phonetics {
		phoneticsQuery.WriteString(v)
	}

	e.Wildcard.Value = phoneticsQuery.String()

	return e.Wildcard
}
